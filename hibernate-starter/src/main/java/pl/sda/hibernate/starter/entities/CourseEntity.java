package pl.sda.hibernate.starter.entities;

import pl.sda.commons.Utils;

import javax.persistence.*;
import java.util.Date;

/*
 * Standardowa klasa typu JavaBean - tego nie narzuca Hibernate, ale jest dobrą praktyką programowania obiektowego.
 */
@Entity
@Table(name="courses")
public class CourseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="name")
    private String name;
    @Column(name="place")
    private String place;
    @Column(name="startDate")
    private Date startDate;
    @Column(name="endDate")
    private Date endDate;

    public CourseEntity( String name, String place, Date startDate, Date endDate) {
        this.name = name;
        this.place = place;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    protected CourseEntity() {}

    public int getId() {
        return id;
    }

    public CourseEntity setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CourseEntity setName(String name) {
        this.name = name;
        return this;
    }

    public String getPlace() {
        return place;
    }

    public CourseEntity setPlace(String place) {
        this.place = place;
        return this;
    }

    public Date getStartDate() {
        return startDate;
    }

    public CourseEntity setStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public Date getEndDate() {
        return endDate;
    }

    public CourseEntity setEndDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }


    //    public CourseEntity(String name, String place, Date startDate, Date endDate) {
//        this.name = name;
//        this.place = place;
//        this.startDate = startDate;
//        this.endDate = endDate;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getPlace() {
//        return place;
//    }
//
//    public Date getStartDate() {
//        return startDate;
//    }
//
//    public Date getEndDate() {
//        return endDate;
//    }
//
//    @Override
//    public String toString() {
//        return "Course{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                ", place='" + place + '\'' +
//                ", startDate=" + Utils.dateFormat(startDate)+
//                ", endDate=" + Utils.dateFormat(endDate) +
//                '}';
//    }
}