package pl.sda.hibernate.starter;

import pl.sda.commons.Utils;

import java.util.Date;

/*
 * Standardowa klasa typu JavaBean - tego nie narzuca Hibernate, ale jest dobrą praktyką programowania obiektowego.
 */
public class Students {
    private int id;
    private String first_name;
    private String last_name;
    private String address;
    private Date born_date;

    /*
     * Jedyny wymóg Hibernate to istnienie bezargumentowego konstruktora (najlepiej z widocznością pakietową albo public!)
     */
    public Students() {}


    public Students(String first_name,String last_name, String address, Date born_date) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.address = address;
        this.born_date = born_date;

    }

    public int getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getAddress() {
        return address;
    }

    public Date getBorn_date() {
        return born_date;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", address='" + address + '\'' +
                ", startDate=" + Utils.dateFormat(born_date)+
                '}';
    }
}