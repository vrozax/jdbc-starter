package pl.sda.hibernate.starter.entities;

import pl.sda.commons.Utils;

import javax.persistence.*;
import java.util.Date;


    @Entity
    @Table(name="students")
    public class StudentsEntity {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="id")
        private int id;

        @Column(name="first_name")
        private String first_name;
        @Column(name="last_name")
        private String last_name;
        @Column(name="address")
        private String address;
        @Column(name="born_date")
        private Date born_date;

        public StudentsEntity setId(int id) {
            this.id = id;
            return this;
        }

        public StudentsEntity setFirst_name(String first_name) {
            this.first_name = first_name;
            return this;
        }

        public StudentsEntity setLast_name(String last_name) {
            this.last_name = last_name;
            return this;
        }

        public StudentsEntity setAddress(String address) {
            this.address = address;
            return this;
        }

        public StudentsEntity setBorn_date(Date born_date) {
            this.born_date = born_date;
            return this;
        }

        /*
         * Jedyny wymóg Hibernate to istnienie bezargumentowego konstruktora (najlepiej z widocznością pakietową albo public!)
         */
        public StudentsEntity() {}


        public StudentsEntity(String first_name, String last_name, String address, Date born_date) {
            this.first_name = first_name;
            this.last_name = last_name;
            this.address = address;
            this.born_date = born_date;

        }

        public int getId() {
            return id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getAddress() {
            return address;
        }

        public Date getBorn_date() {
            return born_date;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "id=" + id +
                    ", name='" + first_name + '\'' +
                    ", last_name='" + last_name + '\'' +
                    ", address='" + address + '\'' +
                    ", startDate=" + Utils.dateFormat(born_date)+
                    '}';
        }
    }

