package pl.sda.jdbc.starter;

//import jdk.swing.interop.SwingInterOpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.Scanner;

public class CoursesManager {
    private static Logger LOG = LoggerFactory.getLogger(CoursesManager.class);

    public void createCoursesTable() {
        ConnectionFactory connect = new ConnectionFactory("/database-sda.properties");
        LOG.info("createsourses Init");
        try (Connection connection = connect.getConnection()) {
            LOG.info("Connected");
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.courses (\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  name VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,\n" +
                    "  place VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,\n" +
                    "  start_date DATE NOT NULL,\n" +
                    "  end_date DATE NOT NULL,\n" +
                    "PRIMARY KEY (id))\n");

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.students (\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  name VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,\n" +
                    "  course_id INT DEFAULT NULL,\n" +
                    "  description VARCHAR(200) DEFAULT NULL,\n" +
                    "  seat VARCHAR(10) DEFAULT NULL,\n" +
                    "PRIMARY KEY (id),\n" +
                    "FOREIGN KEY (course_id) REFERENCES sda_courses.courses(id))\n");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.attendance_list(\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  student_id INT DEFAULT NULL,\n" +
                    "  course_id INT DEFAULT NULL,\n" +
                    "  date DATETIME DEFAULT NULL,\n" +
                    "PRIMARY KEY (id),\n" +
                    "FOREIGN KEY (student_id) REFERENCES sda_courses.students(id),\n" +
                    "FOREIGN KEY (course_id) REFERENCES sda_courses.courses(id))\n");

            LOG.info("Table sda_courses.courses created!");
        } catch (SQLException e) {
            LOG.error("Error during using connection", e);
        }
    }

    public void createStudentsTable() {
        ConnectionFactory connect = new ConnectionFactory("/database-sda.properties");
        LOG.info("createsourses Init");
        try (Connection connection = connect.getConnection()) {
            LOG.info("Connected");
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.students (\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  name VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,\n" +
                    "  course_id INT DEFAULT NULL,\n" +
                    "  description VARCHAR(200) DEFAULT NULL,\n" +
                    "  seat VARCHAR(10) DEFAULT NULL,\n" +
                    "PRIMARY KEY (id),\n" +
                    "FOREIGN KEY (course_id) REFERENCES sda_courses.courses(id))\n");


            LOG.info("Table sda_courses.courses created!");
        } catch (SQLException e) {
            LOG.error("Error during using connection", e);
        }

    }

    public void createAttendanceListTable() {
        ConnectionFactory connect = new ConnectionFactory("/database-sda.properties");
        LOG.info("createsourses Init");
        try (Connection connection = connect.getConnection()) {
            LOG.info("Connected");
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.attendance_list(\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  student_id INT DEFAULT NULL,\n" +
                    "  course_id INT DEFAULT NULL,\n" +
                    "  date DATETIME DEFAULT NULL,\n" +
                    "PRIMARY KEY (id),\n" +
                    "FOREIGN KEY (student_id) REFERENCES sda_courses.students(id),\n" +
                    "FOREIGN KEY (course_id) REFERENCES sda_courses.courses(id))\n");

            LOG.info("Table sda_courses.courses created!");
        } catch (SQLException e) {
            LOG.error("Error during using connection", e);
        }
    }

    public void dropAllTables() {
        ConnectionFactory connect = new ConnectionFactory("/database-sda.properties");
        LOG.info("createsourses Init");
        try (Connection connection = connect.getConnection()) {
            LOG.info("Connected");
            Statement statement = connection.createStatement();
            statement.executeUpdate("drop TABLE attendance_list;");
            statement.executeUpdate("drop table students;");
            statement.executeUpdate("drop table courses;");


            LOG.info("Table dropped!");
        } catch (SQLException e) {
            LOG.error("Error during using connection", e);
        }
    }

    public void printAllStudents() {
        ConnectionFactory connect = new ConnectionFactory("/database-sda.properties");
        LOG.info("printAllstudents Try");
        try (Connection connection = connect.getConnection()) {
            LOG.info("Students list Connected");
            Statement statement = connection.createStatement();
             String query = "SELECT st.*,cs.name, case when st.course_id then cs.name else 'bez kursu' end kurs FROM students AS st  \n" +
                     "                     left join courses as cs ON st.course_id=cs.id;";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
             //   int id = result.getInt("id");
                LOG.info(result.getString("st.name")+":"+result.getString("kurs"));
            }


        } catch (SQLException e) {
            LOG.error("Error during using connection", e);
        }

    }

    public void printAllCourses() {
        ConnectionFactory connect = new ConnectionFactory("/database-sda.properties");
        LOG.info("printAllCourses Try");
        try (Connection connection = connect.getConnection()) {
            LOG.info("Courses  list Connected");
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM courses;";
            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
              //  int id = result.getInt("id");
                LOG.info(result.getString("name")+":"+result.getString("place")+" od:"
                +result.getString("start_date")+" do:"+result.getString("end_date"));
            }


        } catch (SQLException e) {
            LOG.error("Error during using connection", e);
        }

    }
    private void updateNameCourse() {

        System.out.println("Podaj id kursu co chcesz zmienic:");
        Scanner scanner =new Scanner(System.in);
        int changeId=Integer.parseInt(scanner.nextLine());
        System.out.println("Podaj zaaktualizowaną nazwę:");
        String nameCourse=scanner.nextLine();
        System.out.println(nameCourse);
        ConnectionFactory connect = new ConnectionFactory("/database-sda.properties");
        LOG.info("printAllCourses Try");
        try (Connection connection = connect.getConnection()) {
            Statement statement = connection.createStatement();
            String query="UPDATE courses set name='"+nameCourse+"' where id="+changeId+";";
           // String query = "UPDATE courses set name='{}' where id={};";
          //  statement.executeQuery(query);
            try(PreparedStatement statement_prep = connection.prepareStatement(query)){ //parameterIndex zaczyna się od 1!  statement.setInt(1, id);  statement.setInt(2, name);  statement.setDate(3, date);
                statement_prep.executeUpdate(); }


        } catch (SQLException e) {
            LOG.error("Error during using connection", e);
        }

    }



    private void showStudentsinCourse() {

        System.out.println("Podaj id kursu:");
        Scanner scanner =new Scanner(System.in);
        int showId=Integer.parseInt(scanner.nextLine());
        ConnectionFactory connect = new ConnectionFactory("/database-sda.properties");
        LOG.info("showStudents course Try");
        try (Connection connection = connect.getConnection()) {
            Statement statement = connection.createStatement();
            String query="SELECT st.name,cs.name from students AS st "
                    +"JOIN courses AS cs ON st.course_id=cs.id "
                    +"where st.course_id="+showId+";";


            // String query = "UPDATE courses set name='{}' where id={};";

            ResultSet result = statement.executeQuery(query);
            while (result.next()) {
                //   int id = result.getInt("id");
                LOG.info(result.getString("st.name")+":"+result.getString("cs.name"));
            }


        } catch (SQLException e) {
            LOG.error("Error during using connection", e);
        }

    }

    public static void main(String[] args) {
        CoursesManager courses = new CoursesManager();
        //courses.createCoursesTable();
//        courses.createCoursesTable();
//        courses.createStudentsTable();
//        courses.createAttendanceListTable();
        courses.printAllStudents();
        courses.showStudentsinCourse();
     //   courses.updateNameCourse();
     //   courses.printAllCourses();
//        courses.dropAllTables();


    }
}
